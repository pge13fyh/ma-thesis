[[_TOC_]]

# General Introduction to Modeling with Graphs and Networks

## Basic: What are graphs?

Graphs and networks represent data in nodes and edges (cf. next two figures). Modeling with graphs and networks[^1] implies a specific
perspective on the content of interest in the research scope. It means
that the underlying research case can be translated into the structure
of nodes connected by edges. This chapter does not explain the
theoretical background of graphs and networks or what kind of graph
calculations are possible (e.g., cluster analysis or shortest paths).
Instead, this chapter is about the general things that might be
important to consider in the context of research in Spatial
Humanities.[^2]
![A simple data schema of a property graph.
Nodes are entities connected by edges. Edges and nodes
can each have properties. An alternative name for a
property is an attribute.](./01.png)

*Figure: A simple data schema of a property graph.
Nodes are entities connected by edges. Edges and nodes
can each have properties. An alternative name for a
property is an attribute*


![An example of a simple graph with two nodes
connected by an edge. Nodes and the edge each have
individual properties.](./image2.png)

*Figure: An example of a simple graph with two nodes
connected by an edge. Nodes and the edge each have
individual properties.*

![Data schema to represent cities' distance and
population.](./image3.png)

*Figure: Data schema to represent cities' distance and
population.*

![Data graph showing the distance and population
of the two cities: Dresden and Leipzig.](./image4.png)

*Figure: Data graph showing the distance and population
of the two cities: Dresden and Leipzig.*



## What visualizations of graphs are possible in Spatial Humanities?

Graphs can be used in Spatial Humanities to represent entities connected
with all the information embedded in the graph representation. Another
way to use graphs in Spatial Humanities is by combining the graph with a
map and using its location information to localize the nodes
geographically. This combination is especially of interest if the
geographic distribution of a particular topic/content/information/object
and the connection between those are essential. Compare, for example,
the following figure, showing the geographic network of the provenance
of manuscripts that are part of Sir Thomas Phillipps collection (cf. the next figure). This visualization can tell us a lot about many of the manuscripts
owned by Sir Thomas Phillipps came from all over Europe.

![Geographical visualization --
Phillipps-Beatty sample group to 1781 (Burrows, 2017, p. 55).](./image5.png)

*Figure: Geographical visualization -- Phillipps-Beatty sample group to 1781 (Burrows, 2017, p. 55).*

Choosing a geographic network or a simple graph depends on the research
question and the data used in the research project. The benefit of a
non-geographic graph is that it is not geographic. The data at hand
might not offer the information to localize it on a map, maybe because
the data is from places that are fictional or non-existent anymore.
Another thing to be aware of in geographic networks is the level of
granularity to work within the graph. Does the research data work with
one continuous systemization level, or does it mix different spatial
levels? To make these points (and some more) more clearly, here are some
questions to think of:

-   Is all the data at hand, for example, on a city level, or does it
    mix different levels? Are some data on a building level and others
    on a country level?
-   How would it be possible in the geographical network to combine
    these different levels? Countries are commonly represented as
    polygons in a geoinformation system (GIS). A building could be
    reduced to a point, but how to combine those in a geographic
    network?
-   Is all the data from the same time? If it is from a specific period:
    Do all the places mentioned persist to exist for the whole period?
    Or do some vanish, for example, because of a war or an earthquake?
    How to present this temporal information in a graph?
-   Maybe making graph snapshots of the different periods and comparing
    them next to each other would be an option?
-   On what kind of map can the data be plotted?
-   Is the available geographic information equivalent to a contemporary
    cartesian understanding of the earth and its representation? Or
    maybe a historical map would be more appropriate to present the
    knowledge of spatial relations of the data.
-   Is it necessary for the research question to know the spatial
    location and distribution?
-   If a map is still of interest, it is not always necessary to make it
    a geographic network map. Maybe it is sufficient to make it a
    "simple" map and separate networks (cf. Ferreira-Lopes &
    Pinto-Puerto, 2018).

## Typical research questions in Spatial Humanities modeled with graphs

Although numerous topics and research questions might be analyzed with
graphs and networks, throughout the thesis, it became apparent that
specific topics are popular with Spatial Humanists. Here are some topics
that are often covered by Spatial Humanists and how graphs were used in
the context of the research projects.

### Provenance

Provenance is about the history of objects. It takes a closer look at
the chronology of the ownership of historical things. A common question
is: Where was the object located at a specific time, and when did the
ownership change? Investigating the provenance of objects can be of
interest, for example, to pursue colonial legacies of objects owned by
different museums. A graph can trace the movement of such things from
one place/person/institution to another and the specific circumstances
of the move.

Some projects dealing with provenance:

-   Mapping Manuscript Migration (Semantic Computing Research Group,
    2020)
    -   Semantic portal for the exploration of the provenance of
        medieval and early modern manuscripts
    -   <https://mappingmanuscriptmigrations.org/en>

-   Burrows (2017)
    -   The provenance of Sir Thomas Phillipps collection of
        manuscripts. It is one of the largest personal collections.
        <https://tobyburrows.wordpress.com/>

### Social Networks

Social networks are about the connectedness of people, ideas,
information, and knowledge. Some possible questions might be how a
particular scientist got the idea for an invention or how political
views spread worldwide.

Some projects dealing with social networks:

-   Mapping the republic of letters (*Mapping the Republic of Letters*,
    n.d.)
    -   Network correspondence analysis of the intellectuals of the Age
        of Enlightenment of the 17^th^-18^th^ century
    -   <http://republicofletters.stanford.edu/>

-   Early Modern Letter Online (Hotson & Lewis, n.d.)
    -   Online collection of letters from the early modern period.
        Allows one to track back the correspondence network. Includes
        some provenance information.
    -   <http://emlo.bodleian.ox.ac.uk/home>

-   Ferreira-Lopes & Pinto-Puerto (2018)
    -   Analysis of relationships of professionals and patrons in the
        context of the Kingdom of Seville in the late gothic period (cf. next
        figure).
    -   Closely related publication: Ferreira-Lopes et al. (2016)

![Graph model with a total of 1400 nodes and 1787 lines. In this image the thickness of the lines between nodes is
proportionate to the number of relationships connecting them (Ferreira-Lopes & Pinto-Puerto, 2018, p. 10).](./image6.png)

*Figure: Graph model with a total of 1400
nodes and 1787 lines. In this image the thickness of the lines between
nodes is proportionate to the number of relationships connecting them
(Ferreira-Lopes & Pinto-Puerto, 2018, p. 10).*

## General approaches to start a data schema for a graph model

Generally, it is advisable to begin with the research question and the
resource data to develop the ground structure of the model.

Some helpful questions that can help with the first step are:

-   What essential concepts for the research are already apparent in the
    research question?
    -   What is an entity, and what is an attribute? Or: What could be
        represented as such?
-   What kind of questions do you want to get the answers to when you
    have finished the network/graph?
    -   Try to think of some possible answers.
    -   What might these answers look like in a graph?
    -   Work from here to model the schema so that the graph can answer
        that question
The level of detail in the graph model:

-   The kind of research question you ask also gives you an idea of how
    detailed the model should be.
-   What degree of granularity/detail is present in the data? Either
    work with that or try to find additional resources to add detail.

A practical tip to start modeling:

-   Take advantage of tools like Recogito (Austrian Institute of
    Technology, n.d.; <https://recogito.pelagios.org/>)
-   Just upload the content and start tagging to get a feeling of what
    seems essential in the context of the data and the research question
-   This bottom-up approach is the same followed here to create the
    examples presented

## What are some benefits of using graphs in a Spatial Humanities research project?

Many different benefits could be of interest in using graphs in a
research project. Most of them are not specifically just for the
discipline of Spatial Humanities but could provide an answer to some
very Humanities specific problems. The following section wants to point
out some of these benefits.

### Infer new information

Think of the situation where the source used in a research project is
archive records on births, marriages, baptisms, and the like. The data
is ordered chronologically and written in several volumes of books. One
of these records states that person A, with parent X, is baptized on day
Y. Several volumes later, after digitizing all the information, there is
a record of person B, with parent X being baptized on day Z. A and B
are, therefore, siblings. This information might not be that obvious,
with tons of records in a database. With several graph database
management systems, it is possible to define rules and automatically
infer this information (cf. the next Figures).

![Possibility of graph databases to infer additional information, for example by defining rules in typeQL. Graph without the inferred information.](./image7.png)

*Figure: Graph without the inferred information.*

![Possibility of graph databases to infer additional information, for example by defining rules in typeQL. Graph with the inferred infomration.](./image8.png)

*Figure: Graph with the inferred infomration.*

It is not uncommon for historical resources to get lost or destroyed
through the centuries. With the option to infer information, these gaps
could be filled with information found in other sources. This approach
could provide Spatial Humanists with a possible solution for the
incomplete data sources that might be the primary source for the
research project. Or another use case is when the underlying information
is very uncertain. This uncertainty could be met by inferring
information from other sources.

A project worth looking at that takes the option to infer additional
knowledge in a grand style is "The Golden Agent Project" (Digital
History, 2021)[^3]. This project takes a closer look at archival data
from the Netherlands and uses the same procedure described above.

### Computational Methods for Graph Analysis

Other benefits of graphs concern the computational analysis algorithms
that can be applied, for example, with tools like Gephi (see Chapter 5
on tools and technologies). Computational analysis can be beneficial for
significant amounts of data. In these cases, a simple look at the final
graph does not give any detailed information. The things
that can be calculated with algorithms are clusters, density,
centrality, or shortest paths, and many more (Wettlaufer, 2016, p. 9)

### Visual Character of Graphs

The visual nature of graphs is also very attractive for projects. Graphs
are a very visual construct with edges and nodes connected. Sometimes,
it can help to look at a graph to see things of interest. Essential for
that are specific graph visualization algorithms. These often take
advantage of weights added to the nodes and edges. These weights work
like springs pressed together or torn apart to move the nodes around for
a layout that, at the same time, can say something about the content
presented.

### Dynamic and Adaptable

The last benefit mentioned here is the adaptability of graph databases.
Many graph databases allow working on the data schema and adding data
relatively dynamically. In many cases, the research workflow changes
dynamically. An example scenario is adding a new source found during the
research process when the data schema is already developed. This work
environment can interest many Spatial Humanists that prefer a dynamic
research process.

# References
- Austrian Institute of Technology. (n.d.). Recogito Homepage. Retrieved October 4, 2022, from
https://recogito.pelagios.org/
- Burrows, T. (2017). The History and Provenance of Manuscripts in the Collection of Sir
Thomas Phillipps: New Approaches to Digital Representation. Speculum, 92(S1), S39–S64.
- Digital History. (2021, November 1). Leon van Wissen/Veruska Zamborlini/Charles van
den Heuvel: Toward an ontology for archival resources. Modelling persons, objects and
places in the Golden Agents research infrastructure [Billet]. Digital History Berlin. Retrieved August, 20, 2022, from https://dhistory.hypotheses.org/361
- Ferreira-Lopes, P., & Pinto-Puerto, F. (2018). GIS and graph models for social, temporal and
spatial digital analysis in heritage: The case-study of ancient Kingdom of Seville Late Gothic
production. Digital Applications in Archaeology and Cultural Heritage, 9.
- Ferreira-Lopes, P., Pinto-Puerto, F., Jimenez Mavillard, A., & Suarez, J. L. (2016). Seeing Andalucia’s Late Gothic heritage through GIS and Graphs. Digital Humanities 2016:
Conference Abstracts., 501–504.
- Hotson, H., & Lewis, M. (n.d.). Early Modern Letters Online. Retrieved October 3, 2022, from
http://emlo.bodleian.ox.ac.uk
- Mapping the Republic of Letters. (n.d.). Retrieved October 18, 2022, from http://republicofletters.stanford.edu/
- Robinson, I., Webber, J., & Eifrem, E. (2015). Graph databases (Second edition). Sebastopol:
O’Reilly.
- Semantic Computing Research Group. (2020). Mapping Manuscript Migrations – A Digging
into Data project for 2017–2020. Retrieved March, 10, 2022, from
https://mappingmanuscriptmigrations.org/en/
- Wettlaufer, J. (2016). Neue Erkenntnisse durch digitalisierte Geschichtswissenschaft(en)?
Zur hermeneutischen Reichweite aktueller digitaler Methoden in informationszentrierten
Fächern. Zeitschrift für digitale Geisteswissenschaften.

**Footnotes**

[^1]: The guide will not differentiate between graphs or networks.

[^2]: For a more detailed introduction to the theory of graphs look for
    publications about graph theory and graph databases. For example:
    Robinson et al. (2015)

[^3]: Project website: <https://www.goldenagents.org/> (accessed:
    18.10.2022); Also worth watching:
    <https://www.youtube.com/watch?v=lqja9wMff6c> (accessed: 18.10.2022)
