[[_TOC_]]

# Bottom-Up Modeling Approach 

The bottom-up approach starts with the data. It develops a model from
the information uncovered in the data. Cole and Hahmann (2019, p. 41)
call this approach *Fist Principles Modeling*. According to them, the
motivation to use First Principles Modeling is to meet the challenge in
the Humanities to model complex and uncertain information that is very
common in this discipline. Therefore, capturing the qualitative nature
of the research data becomes the focus of this modeling approach.

## Close up at a Bottom-Up Approach: Qualitative Spatial Representation - Regional Connection Calculus and Semantic Triples.

The bottom-up approach of Qualitative Spatial Representation (QSR) puts
into focus the relational aspect, which is also key to the graph
representation of information (Stell, 2019, p. 4). Stell (2019)
introduced QSR in the context of the Humanities. He points out two
features of QSR: First, it deals with qualitative spatial relations.
Second, it allows for digital representation. One approach that Stell
presents to describe qualitative spatial relations is Regional
Connection Calculus (RCC). RCC defines different kinds of regional
relations of two entities in a structured way (see the next table for an
overview of types of regional relations). RCC can describe any kind of
relationship, not only spatial ones but also the arrangements and
patterns of text, as presented in Stell (2019, p. 21).

*Table: Defining relationships in the Region-Connection Calculus from connection (Stell, 2019, p. 16).*

 | Relation of x to y         | Definition in terms of connection and derivatives                                 |
|----------------------------|-----------------------------------------------------------------------------------|
| Part of                    | Every region connected to x is also connected to y                                |
| Equal to                   | x is a part of y, and y is a part of x                                            |
| Overlaps                   | There is a region which is part of x and also part of y.                          |
| Partially overlaps         | x overlaps y but neither x nor y is a part of the other                           |
| Proper part                | x is a part of y and y is not a part of x                                         |
| Externally connected       | x is connected to y but they do not overlap                                       |
| Non-tangential proper part | x is a proper part of y and no region z is externally connected to both x and y   |
| Tangential proper part     | x is a proper part of y and some region z is externally connected to both x and y |


These relations can be translated to basic information snippets
corresponding to RDF triples subject-predicate-object (e.g., Hanna
\[subject\] -- lives-in \[predicate\] -- Bologna \[object\]). This way,
qualitative spatial relations were modeled by Murrieta-Flores et al.
(2019). They used QSR and RCC to describe spatial representation on
early colonial Mexican maps (cf. the next two figures).

The created network by Murrieta-Flores et al. (2019) can describe
spatial and qualitative relations presented in a historical map. They
state that this could create a generally applicable knowledge base for
many colonial maps. That knowledge base includes what is printed on the
map and "all that is known" about the social, cultural, political, and
many more aspects that constructed the place represented on a map
(Murrieta-Flores et al., 2019, pp. 63, 66).

This first principles modeling of information in the context of Spatial
Humanities allows for very detailed and multi-layered modeling of Places
of any kind and nature. It will enable researchers to focus on what
matters instead of worrying about a specific data schema. The model is
adaptable, no matter what information is used as a research source:
text, images, or objects. Also, the linkage to the Semantic Web is not
lost in that way. When following the simple RDF structure, the
publication of the information online is possible.


![Semantic Network showing only few components combining QSR and other attributes associated through semantic triples (Murrieta-Flores et al., 2019, p. 64).](./image14.png)

*Figure: Semantic Network showing only few components combining QSR and other attributes associated through semantic triples (Murrieta-Flores et al., 2019, p. 64*
  
![Map of Atengo-Misquiahuala, with permission of the Benson Latin American Collection, LLILAS Benson Latin American Studies and Collections, The University of Texas at Austin (Murrieta-Flores et al., 2019, p. 56).](./image15.png)

*Figure: Map of Atengo-Misquiahuala, with permission of the Benson Latin American Collection, LLILAS Benson Latin American Studies and Collections, The University of Texas at Austin (Murrieta-Flores et al., 2019, p. 56).*


# Examples of a Bottom-Up approach:

## Example 1: RCC modeling

This example takes a closer look at RCC and how it could be used to
model Place information in spatial narratives. This example uses a
different source type than presented by Murrieta-Flores et al. with
their early colonial Mexican map. The use case is from the third chapter
of *Around the World in Eighty Days*[^1] by Jules Verne, which was
first published in 1873. The chapter describes Phileas Fogg's day at the
Reform Club and how he came to wager that he could travel the world in
80 days. This chapter was chosen because it shows the expression of
distance in time and the different uses of relations on a social and
spatial level.

Again, to gain an overview of the content, the tool Recogito is used.
The modeling part will mainly focus on these parts that allow
exemplifying the use of RCC, meaning that not the complete chapter is
represented in the following model example. The chosen part is an
exemplary display of using time to communicate distances in the 19^th^
century.

![Data schema model for the RCC example based on the information presented in the previous table.](./image16.png)

*Figure: Data schema model for the RCC example based on the information presented in the previous table.*

The previous figure shows the data schema that was developed for the RCC example.
As can be seen, the actors/entities found in the text each play one role
in the relations. The relation-type "disconnected" is added to the
schema to describe when things are explicitly mentioned as having no
connection. This one relation is added to model the relationship of the
thief who has stolen a significant amount of money from the Bank of
England and is not part of a gang of thieves. The relationship
"externally connected" has the attribute "distance" to model the
mentioned days of travel time it takes to go from one place to another.

After editing the whole chapter and connecting the entities with their
according relations, the entire graph looks a bit overwhelming. The
graph does not tell the viewer anything but that there are a lot of
connected entities (cf. the next figure). But with just a few simple queries,
it is possible to extract different kinds of relations. Some of them are
presented below.

![RCC graph for the third chapter of Jule Verne's "Around the World in 80 days](./image17.png)

*Figure: RCC graph for the third chapter of Jule Verne's "Around the World in 80 days"*

A couple of example queries below exemplify the possibilities of RCC in
a graph. The first one shows all the rooms that are part of the Reform
Club building described in the chapter (cf. figure "Rooms in the Reform Club"). The next one lists
all the members that are part of the Reform Club (cf. figure "Members of the Reform Club"). These two examples show
the different kinds of relations that RCC can model. The model can
present spatial relations and social relations (membership to the Reform
Club). This option helps to model the social part of the construction of
Places, which plays a crucial role in the concept of Place as it is
understood in Human Geography.

The second group of queries is about the distances in travel days
mentioned in the chapter. The third query shows nicely how all the places are
connected circularly. If interested in the greater distances that take
more than ten days, this can also be queried (cf. figure "Places that are further than ten days travel apart").

Overall, as seen from these few example queries, RCC is an easily
applicable way to model types of qualitative spatial and social
relations mentioned in fiction.


![Rooms of the Reform Club](./image18.png)

*Figure: Rooms of the Reform Club.*

``` 
#get all instances that are a proper part of the Reform Club         
match $x isa actor, has name $n1;                                  
$y isa actor, has name $n; $n= "Reform Club";                   
$r (x:$x, y:$y) isa proper_part;                                 
get $r, $n1, $n;    
```                                               




![Members of the Reform Club.](./image19.png)

*Figure: Members of the Reform Club*
```
# get all the members of the Reform Club
match $x isa actor, has name $n1;
$y isa actor, has name $n; $n= "Reform Club";
$r (x:$x, y:$y) isa part_of;  
get $r, $n1, $n;
``` 

![Overview of the days of travel that the places mentioned in the third chapter are apart](./image20.png)

*Figure: Overview of the days of travel that the places mentioned in the third chapter are apart.*

```
#get all the instances that have a distance = the journey around the
world                                                                
match $x isa actor, has name $n1;                                  
$y isa actor, has name $n2;                                        
$r (x:$x, y:$y) isa externally_connected;                         
$r has distance $d;                                                
get $x, $y, $n1, $r, $d, $n2;    
```

![Places that are further than ten days travel apart.](./image21.png)

*Figure: Places that are further than ten days travel apart.*

```
match $x isa actor, has name $n1;                                 
$y isa actor, has name $n2;                                     
$r (x:$x, y:$y) isa externally_connected;                      
$r has distance $d;                                             
$d>10;                                                          
get $x, $y, $n1, $r, $d, $n2;
```

## Example 2: Semantic Triples in RDF modeling

This example takes a closer look at semantic triples, their basic
structures of subject-predicate-object, and how they can be used to
model platial information. The exemplary use case chosen here is the
same letter used to create the field map in the chapter about the agent based approach. The exact use case was
chosen to highlight the different information achieved by a different
modeling approach. The modeling approach closely follows the procedure
presented by Cole and Hahmann (2019). This approach means that there are
classes, entities can be instances of a specific class, and the entities
are connected by properties that describe the relationship between the
two entities.

An overview of the content was created with Recogito. Then all the
entities and relations were added to make a semantically enriched graph
representation of Charles Darwin's letter to Syms Covington, as with all
the previous examples. The underlying data schema can be seen in the next figure. It shows that the main classes are entities, and sub-entities are
persons and place names. Them being sub-entities means that there is an
inheritance, and all the attributes and relations owned by the parent
are inherited by the child/sub-entity. They all possess a description
attribute, and a date can be added. The connection was modeled in an
accurate semantic triple manner, with a subject and predicate being one
of the entity classes and the relation.


![Data schema for the semantic triple model of the letter from Charles Darwin to Syms Covington.](./image22.png)

*Figure: Data schema for the semantic triple model of the letter from Charles Darwin to Syms Covington.*


With the just presented data schema, the semantic triple statements can
be created based on the letter\'s content. The complete graph with all
the developed relations is messy, like the previous graph in the previous example.
Therefore, only some queries that show the possibilities of this
semantic triples-based modeling approach are presented.


![Complete graphe for example 2 with semantic triples.](./complete-graph.png)

*Figure: Complete graphe for example 2 with semantic triples.*


Ego networks are suitable when it is obvious from the beginning that
there are central entities of interest. Another is when the focus of the
research question is to see what is associated with these. Two of the
queries below show the ego networks of the protagonists of the letter:
Charles Darwin and Syms Covington (cf. the next two figures). In the
context of the presented example, Charles Darwin's ego network is much
bigger than the one from Syms Covington. Because the graph is based on a
letter Darwin wrote to Covington, it makes sense that Darwin writes much
more about himself than about Covington.

When modeling the relations predicates for this example, they were
chosen based on what made the most sense in the context of the relation
intended to create. This model led to a significant number of different
predicates. The upside of this approach is to model very closely to what
information is available in the data. A downside of this approach is the
variety of predicates that are maybe similar but not quite the same. It
is questionable if the many varying predicates are applicable to
meaningful queries. The type of the predicates and the descriptions are
all the type *string*. This data type is bound to be a source of typos
and errors. In some use cases, it might make sense to keep that in mind
and change the data schema accordingly. It is worth mentioning that
different types allow different kinds of operations. In the Jules Verne
example, one of those operations was already used in the query to show
all the places that take more than ten days of traveling time.

![Charles Darwin's ego network](./image23.png)}

*Figure: Charles Darwin's ego network.*

```
#query to Charles Darwin's ego network
match $p isa person;
$p has description "Charles Darwin";
$r (subject: $p, object:$x) isa rel;
$r has predicate $pr;
$p has description $d;
$x has description $d1;
get $p, $d, $r, $d1, $pr; 
```

![Syms Covington's ego network.](./image24.png)

*Figure: Syms Covington's ego network.*

```
#query to Syms Covington ego network
match\$p isa person\                      
$p has description "Syms Covington\                
$r (subject:$p, object:$x) isa rel                
$r has predicate $pr;              
$p has description $d;                     
$x has description $d1;                          
get $p, $d, $r, $d1, $pr;
``` 

This letter model only contains information found in the letter. Adding
information associated with the content would be possible. This
information could be manifold: the biographical data from when Charles
Darwin and Syms Covington lived; when they were together on the HMS
Beagle; the link to the resources from which this information was taken,
and many more. This adaptability makes the semantic triple model very
easily adaptable and expandable. Still, it should be used cautiously so
as not to overly add many pieces of information and not to get
sidetracked but to keep the focus on the main research questions.

Unlike the field map, following the subject-predicate-object pattern on
the letter can easily add more contextual information that is tightly
connected to the data content. But this does not mean that the field
maps should be discarded, but that field maps and the semantic triple
approach fulfill different needs. The idea of the field maps is
basically to create clusters, which is done by associating the actors to
fields. The idea of the subject-predicate-object pattern is to take a
closer look at the kinds of relationships. The field map allows the
modeling of subject-object relations. It could be much harder to find
bigger clusters in the semantic triples by simply glancing at them
without using a network analysis algorithm (e.g., with the tool Gephi,
as previously done). Therefore, comparing the two highlights that
although the data source is the same, different modeling approaches are
related to the research question.

![Crew Members of the HMS Beagle.](./image25.png)

*Figure: Crew Members of the HMS Beagle.*

```
#get the crew members of the HMS Beagle
match $p isa ent;
$p has description "HMS Beagle";
$x isa person;
$r (subject:$x, object:$p) isa rel;
$x has description $d;
get $p, $x, $r, $d; 
```

# Take away messages from a bottom-up modeling approach:

-   Allows modeling of very complex scenarios
-   With the basic structure of semantic triples in a RDF format, it is
    possible to upload the research results in a sharable way to
    contribute to the semantic web
-   Applicable to any understanding of Place
    -   Allows modeling of non-cartesian and non-Euclidean understanding
        of Place
    -   Allows modeling social relations
-   Applicable to any kind of resource
-   Allows modeling on any zooming scale
    -   Country-, street-level, or anything else, everything can be
        incorporated into one model
-   Allows to model with the flow of the research project
    -   No need to adapt the data structure if new information needs to
        be incorporated
-   Allows to use of the format that is inherent in the data for the
    model
-   Very close to working with the data itself
-   It might be too complex and too domain-specific to make the results
    usable for other research projects
-   Applying the model to a significant amount of research data is
    probably very time-consuming if automation of the process is
    complicated/not possible
-   Helpful in an exploratory stage of a project

# Projects that apply bottom-up modeling approaches to take a closer look at:

-   Murrieta-Flores et al. (2019)
    -   QSR approach -- RCC and semantic triples
    -   Analysis of the spatial representation of historical maps
-   Cole & Hahmann (2019)
    -   QSR approach -- semantic triples
    -   Analysis of spatial narratives of victims of the Holocaust
-   Palladino (2021)
    -   Analysis of the spatial narratives in Greco Roman
    -   Not solely a bottom-up modeling approach because it takes
        advantage of the implied structure represented by language
    -   Palladino frames the model as a meta-model

# References 
- Cole, T., & Hahmann, T. (2019). Geographies of the Holocaust: Experiments in GIS, QSR,
and Graph Representations. International Journal of Humanities & Arts Computing: A Journal
of Digital Humanities, 13(1–2), 39–52.
- Murrieta-Flores, P., Favila-Vázquez, M., & Flores-Morán, A. (2019). Spatial Humanities 3.0:
Qualitative Spatial Representation and Semantic Triples as New Means of Exploration of
Complex Indigenous Spatial Representations in Sixteenth Century Early Colonial Mexican
Maps. International Journal of Humanities & Arts Computing: A Journal of Digital Humanities,
13(1–2), 53–68.
- Palladino, C. (2021). Mapping the unmapped: Transmedial representation of premodern
geographies. Berichte Geographie Und Landeskunde, 94(2), 139–160.
- Stell, J. G. (2019). Qualitative Spatial Representation for the Humanities. International Journal
of Humanities & Arts Computing: A Journal of Digital Humanities, 13(1–2), 2–27.

**Footnotes**
[^1]: Source <https://www.gutenberg.org/files/103/103-0.txt> (Accessed: 25.10.2022).

