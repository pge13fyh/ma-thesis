# Event- and Agent-Based Modeling Approach

Event-based and Agent/Actor-based modeling use, as their names already
suggest, events and agents/actors as their central concepts to create a
data model. In the projects analyzed, the event-based modeling approach
dominates because many projects use the even-centered ontology
CIDOC-CRM[^1] for their projects. This section wants to take a closer
look at why concepts like events and agents help to create data models.

## Event-based modeling: CIDOC-CRM

The event-based or event-centric modeling approach is naturally of
interest for most data in the humanities. Most of the research data
originate from the past. The data are attestations of past things and
events that have happened. This attribution already points out the
natural way of thinking of data from the past as events. It allows us to
define certain historical realities around these events of interest
(Bruseker et al., 2017, p. 114). The idea of an event does not require
everything about the event itself to be known. Events allow for
uncertainties. For example, events could be known precisely when and
where they happened. If it is not known, it is always possible to go
down several levels of granularity and say that it occurred within a
specific month, year, or period. Additionally, events allow us to
connect information through events. To give an explicit example: We have
the letters of two people. Both have witnessed the Storming of the
Bastille and have written about it to their families. These two letters
present different perspectives on the event, but because both letters
describe the same event, it allows us to connect these experiences of
two different people.

CIDOC-CRM is a top-level ontology. The ontology takes advantage of the
fact that anything of the past can be modeled as an event. This approach
is also apparent in the top-level categories, where the temporal entity
is the center of all and the one with the most connection to the other
top categories (cf. next figure)).

![CIDOC-CRM Top Level Categories (Bruseker et al., 2017, p. 112).](./image9.png)

*Figure: CIDOC-CRM Top Level Categories (Bruseker et al., 2017, p. 112).*

## Projects applying an event-based modeling approach

-   Hübl & Scholz (2021)
    -   Extend the Linked Pasts ontology by the Linked Traces format for
        the attestation of historical events. Closely related to the
        Linked Places format developed by Grossner and Mostern (2021)
        that is implemented in the World Historical Gazetteer.[^2]
-   Koho et al. (2022)
    -   Describes the development of a data model to be used in the
        *Mapping Manuscript Migration* portal about the provenance of
        European medieval and Renaissance manuscripts. The project
        applies the event-centric CIDOC-CRM ontology.
-   Horne (2021)
    -   This project applies network analysis to determine the power of
        empires of Hellenistic Anatoli from the reign periods of several
        Attalid kings.
-   Burrows (2017)
    -   Analysis of the provenance of the Sir Thomas Phillipps
        collection.

# Actor/Agent-based modeling approach

Actor-network-theory originally comes from the social sciences and takes
a closer look at the interwoven relationship of humans and
(non-)material things. Harris Cline (2020, p. 230) uses this to take a
closer look at archeological findings and their relation to human
society and activities. She does this by creating field maps. Field maps
are very similar to mind maps. The fields resemble overarching topics,
and the information found in the resource is then associated with these
fields. The topics can be anything that makes sense in the context of
the source and research question. Associating information to these
fields automatically creates clusters. Another benefit is the
possibility of seeing some entities\' entangled relations in the
network. Additionally, from the beginning, the researcher must think
from the bigger picture of the fields and how these fields play a role
in everything incorporated into the network. Last, field maps are
abstract and not attached to any geographic representation. This
abstraction allows fields of imaginary or fictional Places to be
included in the field maps that are not mappable on a conventional map.

A practical downside to the fields is the lack of dynamic adaptability.
Once the fields are created, adding another field in the middle of the
project is complex. Adding a new field implies checking all the added
entities to see if they relate to the new field. Another downside of the
field map presented by Harris Cline is the lack of directed edges and
properties that can be added to the edges. These could add further
information about the types of relations between the nodes in the
network.

## Example of a field map inspired by Harris Cline (2020)

This practical example of a field map emphasizes the entanglement of
Place in different fields. The field map is based on Charles Darwin\'s
letter to Syms Covington from 1852.[^3] This little experiment aims to
investigate everything entangled with Australian colonialism and its
gold rush happening at that time from Charles Darwin's perspective.
Additionally, other topics, or in the context of this modeling method
called fields, that are exchanged between Syms Covington and Charles
Darwin can be discovered.

![ Annotated letter from Darwin to Syms Covington in Recogito.](./image10.png)

*Figure:  Annotated letter from Darwin to Syms Covington in Recogito.*

The first step involved reading the letter and starting to tag the
content with topic/field tags. This was an iterative process because
before the fields could be chosen, the content had to be checked to see
what general topics could be extracted. For this process, the annotation
tool Recogito was used.[^4] Recogito was chosen because it has an easy
on-the-fly option to annotate text data and allows adding
relation-annotation to the text, therefore connecting parts. This tool
was used to relate the annotations with their according field and to
other actors. After the annotation was done and the relation of the
annotated tag parts was added, two CSV files could be exported. One file
for the nodes and another for the edges. These files could be imported
directly into Gephi.[^5]

A little note on the practicability of Recogito in this use-case: For
this little use case of just a single letter, Recogito was a good tool
to use, but as can be seen in the previous figure, it gets a bit messy with a
significant number of relations added. Especially because errors can
happen quickly, and it is not possible in Recogito to see and edit the
lists of annotations except in the graphical user interface where the
text is annotated as well. Therefore, another tool should be considered
for a bigger dataset, for example, creating a spreadsheet for the nodes
and edges straight from the text.

The final field map can be seen in the next figure. It shows the fields in
green and the actors associated with them in pink. For the graph layout,
the commonly applied force algorithm was chosen that draws connected
nodes closer together and those not connected further apart. From the
field map, it is evident that most nodes relate to the actor Charles
Darwin, with him being the person who wrote the letter. The field map
presents a humorful and distancing view of Charles Darwin\'s perspective
on the colonies, but Darwin is nevertheless interested. He explicitly
states that the English people are proud of the Australian colony, not
himself, and he further writes that he is amused to hear about how
people behave in the colony. Otherwise, most information about the
Australian colony deals with the gold rush during the time of the
letter.

  ![Simple field map of one letter from Charles Darwin to Syms Covington. The colors of the nodes represent the type -- green for a field and pink for an actor.](./image11.png)

  *Figure: Simple field map of one letter from Charles Darwin to Syms Covington. The colors of the nodes represent the type -- green for a field and pink for an actor.*

Another field map visualization can be seen in the next figure. This
visualization already takes advantage of some of the statistical
analysis calculations offered by tools like Gephi. For the
visualization, the clusters were calculated. The clusters are used to
color the nodes according to their cluster group and degree. The number
of edges attached to a node determines the degree. This information is
used to calculate the size of the nodes. These kinds of additional
visualization attributes allow for the visualizations of graphs to be
much richer in information that can be taken from an image in an
instance. The cluster analysis was done to see if a statistical analysis
of these clusters would group the nodes to the same fields, as was done
by hand.

  ![Field map visualized depending on the degree and cluster analyzed by Gephi statistics.](./image12.png)

*Figure: Field map visualized depending on the degree and cluster analyzed by Gephi statistics.*

The little experiment presented here showed that field maps are easy to
create. They allow for a general overview of a particular matter and a
feeling of the entanglement of specific data/information. But it should
be noted that this only gives a general overview and that specific
contextual details are lost in creating the field maps. Nevertheless, in
the context of the presented use case, such a field map could be of
interest to make one map for each person that Charles Darwin
corresponded with. That would allow the comparison of the different
fields/topics discussed by Charles Darwin with others.

Regarding the Place information that a field map presents, the field
place chosen here is used to connect place names as a starting point.
From the field map, it is visually apparent that these places are
connected to actors, and these actors are connected to other actors and
fields. Although Place seems to be reduced to place names, the deep
entanglement of these Places is not lost.

# Takeaway messages for field maps in Spatial Humanities research projects:

-   Great to create topic-/field-based overviews of information
-   Easy to make with the tools Recogito and Gephi (for a small data
    set)
-   Loss of contextual information
-   Quick overview of the entanglement of Places/information generally
    with different fields and actors

# Projects applying an agent/actor-based modeling approach:

-   Harris Cline (2020)
    -   Creates field maps with central fields/actors
    -   Inspiration for the presented example
-   van Bree & Kessels (2015)
    -   Analysis of memories of the anti-communist violence in Semarang,
        Indonesia, in 1965


# References
- Bruseker, G., Carboni, N., & Guillem, A. (2017). Cultural Heritage Data Management: The
Role of Formal Ontology and CIDOC CRM. In M. L. Vincent, V. M. López-Menchero
Bendicho, M. Ioannides, & T. E. Levy (Eds.), Heritage and Archaeology in the Digital Age (p.
93–131). Basel: Springer International Publishing.
- Burrows, T. (2017). The History and Provenance of Manuscripts in the Collection of Sir
Thomas Phillipps: New Approaches to Digital Representation. Speculum, 92(S1), S39–S64.
- Harris Cline, D. (2020). A Field Map for Untangling the Entangled Sea. Journal of Eastern
Mediterranean Archaeology and Heritage Studies, 8(3–4), 226–249.
- Horne, R. (2021). Digital Tools and Ancient Empires: Using Network Analysis and
Geographic Information Systems to Study Imperial Networks in Hellenistic Anatolia.
Journal of World History, 32(2), 321–343.
- Hübl, F., & Scholz, J. (2021, December 13). Spatial Linked Data Approach for Trace Data in
Digital Humanities. Spatial Data Science Symposium 2021 Short Paper Proceedings. Spatial Data
Science Symposium 2021, Online.
- Koho, M., Burrows, T., Hyvönen, E., Ikkala, E., Page, K., Ransom, L., Tuominen, J., Emery,
D., Fraas, M., Heller, B., Lewis, D., Morrison, A., Porte, G., Thomson, E., Velios, A., &
Wijsman, H. (2022). Harmonizing and publishing heterogeneous premodern manuscript
metadata as Linked Open Data. Journal of the Association for Information Science and
Technology, 73(2), 240–257.
- van Bree, P., & Kessels, G. (2015). Mapping Memory Landscapes in nodegoat. In L. M.
Aiello & D. McFarland (Eds.), Social Informatics (Vol. 8852, pp. 274–278). Basel: Springer
International Publishing.

**Footnotes**
[^1]: CIDOC-CRM Website: <https://cidoc-crm.org/> (Accessed: 19.10.2022)

[^2]: World Historical Gazetteer website: <https://whgazetteer.org/>
    (Accessed: 19.10.2022)

[^3]: The complete letter can be found in the GitLab repository.

[^4]: Recogito is a tool developed within the Pelagios Network (see
    also section 6 on tools for graphs and networks). Website:
    <https://recogito.pelagios.org/> (Accessed: 20.10.2022). The link to
    the Recogito project, where the annotations can be seen:
    <https://recogito.pelagios.org/document/b6zmhgwutbsrwc> (Accessed:
    27.11.2022).

[^5]: It is possible to simply reproduce the visualizations by opening
    the Gephi projects that are available in the GitLab repository.
