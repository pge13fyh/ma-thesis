[[_TOC_]]
# Proposed Approach to Capture the Complexity of Place

This little part will take up the proposed approach that results from
the discussion of the master thesis. The approach results from the bits
and pieces of the previously discussed approaches to place. Hence,
combining the best of event/actor-oriented, top-down, and bottom-up
modeling approaches. This part will not include a full-on example but
will be more of a tips and hints collection that takes what was learned
from previous approaches.

The overarching idea of the proposed approach is to embrace how
researchers of the humanities work. It is inspired by Van den Heuvel's
approach that he framed Deep Networks (2020) and the HistoGraph
visualization tool developed by Novak et al. (2014).[^1] Research in
the humanities and historical science is hermeneutical, iterative, and
non-linear (Novak et al., 2014, p. 244). It is proposed to use a very
flexible data model from the beginning to implement this working style
in a research project that wants to investigate Place with graphs and
networks. Depending on how the project evolves, additional information
might have to be incorporated, or some data must be discarded. And as
could be seen from the two bottom-up approach examples with the field
maps and modeling with QSR: This approach does not mean any viable
information is lost. But from the information structured in the
beginning, more structure can be incorporated into the data schema
iteratively. The information can be easily uploaded into the semantic
web using simple structures like RDFs. Additionally, linked open data
can be incorporated to enrich the underlying resources. It is advised to
work closely with the source material. One way to do so is presented in
the tool HistoGraph, where the references associated with the selected
node in the graph are listed next to it (cf. next figure).

![The ego-network of Konrad Adenauer in the prototype of HistoGraph.](./image26.png)

*Figure: The ego-network of Konrad Adenauer in the prototype of HistoGraph.*


Van Den Heuvel proposes the overlay of different networks of actors and
entities to understand the data from different perspectives (Van Den
Heuvel et al., 2020, p. 202). Additionally, a mix of methods is proposed
to allow different perspectives on the use-case project. Graphs and
networks, although in many cases very intuitive and informative, are not
the answer to every use case. Sometimes a simple geographic map or bar
charts of the number of tags added to a text source will do. This part
wants to encourage to explore and experiment. It is essential to try out
different methods and approaches and figure out by simply using them if
they work for the project. Experimenting with the data allows us to
explore the data and its content. Therefore, give it a try, no matter
how absurd it might seem at the beginning. Of course, this is not an
invitation to just do something with the data and then leave it at that.
Instead, it is an invitation to be open-minded to unusual approaches
and, simultaneously, to be transparent about the applied methods. It is
no use for anybody to see a finished product and not know how it came to
be, especially when it is about complex data from the humanities.

The proposed approach is momentarily better applicable to a relatively
small data set. So far, no tools are available that can meet the need to
create computer-assisted deep networks for large data sets (Van Den
Heuvel et al., 2020, p. 206).

## Take away message from the proposed modeling approach

-   Use a data structure that allows for embracing the humanistic
    approach to research
    -   A data structure that would enable dynamic/easy editing
-   Experiment with different methods
-   Be transparent about the sources used, the modeling, and the
    methodological approach taken
-   Be transparent about the underlying definitions used for the data
    structure

# References
- Novak, J., Micheel, I., Melenhorst, M., Wieneke, L., Düring, M., Morón, J. G., Pasini, C.,
Tagliasacchi, M., & Fraternali, P. (2014). HistoGraph – a visualization tool for collaborative
analysis of networks from historical social multimedia collections. 18th International
Conference on Information Visualisation, 241–250.
- Van Den Heuvel, C., van Vugt, I., van Bree, P., & Kessels, G. (2020). Deep networks as
associative interfaces to historical research. In F. Kerschbaumer, L. von Keyserlingk, M.
Stark, & M. Düring (Eds.), The power of networks: Prospects of historical network research.
London: Routledge.

**Footnotes**
[^1]: Not to be confused with Deep Neural Networks from artificial
    intelligence.
