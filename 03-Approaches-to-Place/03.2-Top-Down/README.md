[[_TOC_]]
# Top-Down Modeling Approach

A top-down modeling approach starts from a specific data schema format
or ontology that already exists at the beginning of the project. The
existing data schema at the beginning does not have to be used as it is.
The data schema is adapted for the underlying domain, data, and project.
Then the research data is modeled accordingly.

Most projects either use the presented top-ontology CIDOC-CRM or the
modeling approach proposed by the Pelagios Network (Linked Pasts format,
Early Modern Places format). CIDOC-CRM was already covered to some
extent in the previous section as an example of an event-based modeling
approach. Therefore, this section will focus on the Pelagios Network
approach Linked Pasts.

But before doing a little deep dive into Linked Pasts data model, some
of the motivations for a top-down approach are presented:

-   The data models are already created for a specific purpose
-   Greater possibility of interoperability with other projects
-   Easier to share project results with others
    -   For example, in one of the Linked Open Data portals like *Early
        Modern Letters Online* or the *World Historical Gazetteer*
-   Many models already consider the publishing of the data on the
    Semantic Web that is important when publishing the research results
    online

So overall, the motivation of projects to use a more top-down modeling
approach is making their research outcomes available in the context of
the Semantic Web and Linked Open Data.

## Linked Pasts Ontology: Linked attestations of Places

The Linked Pasts and its connected World Historical Gazetteer (WHG) work
closely with the Pelagios Network. They all follow the idea of creating
a knowledge base about a place with a collection of attestation from
historical sources linked to Places (Kahn et al., 2021, p. 94). The
Linked Pasts data format consists of the two formats for Linked Places
and Linked Traces.[^1] Linked Places aims to be as much interoperable
as possible and seeks to embrace the multiple ways that place
information is available. A place attestation only requires a minimum of
a name, type, and a time reference. In contrast, everything else is
optional, and as can be seen by the added cardinalities to the model,
there can be as many of them as one wishes (cf. next figure). This
flexibility offers the option to add alternative names and alternative
locations. Combining Linked Places with the Linked Traces format, it is
possible to 'link places together to create spatially explicit
historical narratives' (Grossner et al., 2022, p. 4). So, at the same
time, as the Linked Pasts format aims to be as generic and interoperable
as possible, it offers the opportunity for researchers to add as much
additional and contextual information as possible. It allows linking it
to other historical sources related to a place. These linkages create a
network of place narratives of connected places and traces other
researchers can use.

![Conceptual model of a Place attestation in Linked Places format (Grossner & Mostern, 2021, p. 41).](./image13.png)

*Figure: Conceptual model of a Place   attestation in Linked Places format (Grossner & Mostern, 2021, p. 41).*

What does this format do for Place research? This format offers
researchers to work as freely with platial information as possible. It
does not imply a specific understanding of Place. Instead, it reduces it
to something that all Place constructs have in common: a human
attestation of meaning and information. Other than that, this format
allows researchers to use this format, to integrate the data into any
underlying Place construct used in the context of their research
project.

*Table: Overview of ontologies and data
schemas used in Spatial Humanities Research.*


| Name of Ontology/Data Model Approach/Data Formats                  | Short Description                                                                                                                                                                                         | Example Project                                                                                                       |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| CIDOC-CRM CIDOC-CRMgeo                                             | Link the CICOC CRM to the GSC GeoSPARQL schema, event-based modeling approach.  Top-Level Ontology                                                                                                        | Ducatteeuw (2021);   Frank & Barget (2021);   Gkadolou & Prastacos (2021);   Görz et al. (2021);   Koho et al. (2022) |
| Linked Pasts Ontology (Linked Places Format, Linked Traces Format) | Places of attestation. Linking different gazetteers in a standardized way. Part of the Linked Pasts ontology. Used for the World Historical Gazetteer.  Part of the Pelagios Network. Linked Data format. | Grossner & Mostern (2021);   Hübl & Scholz (2021)                                                                     |
| Early Modern Places                                                | A data format designed for letters from the early modern period.  Linked Data format                                                                                                                      | Bosse (2019)                                                                                                          |
## Take away messages from a top-down modeling approach:

-   Most of the developed formats and ontologies have been tested and
    are already widely used and accepted as a standard in the research
    community
-   Give guidance on how to model the data
-   Shareability, interoperability
-   Not necessary to reinvent the wheel
-   It may not apply to a particular use case



## Projects that apply top-down modeling approaches to take a closer look at:

-   Early Modern Places format (Bosse, 2019)
    -   Developed for the Early Modern Letters Online portal
    -   Related modules are the Early Modern Dates and soon-to-come
        Early Modern Peoples format
    -   Supports the Linked Places format (Bosse, 2019, p. 84)
    -   It is part of the Pelagios Network Group
-   Ducatteeuw (2021) creates a digital urban gazetteer
    -   Combines Linked Places Format, CIDOC-CRM classes, and GeoSPARQL
        standard
    -   Linked Places format for interoperability
    -   CIDOC-CRM Centered around the class Spacetime Volume
        (event-based modeling)
    -   GeoSPARQL standard for operability in GIS
    -   Place information in the gazetteer based on observation in
        sources -- like attestations in Linked Places Format
-   Gkadolou & Prastacos (2021)
    -   Development of a data management tool for a historical map from
        Crete in Arches[^2]
-   Görz et al. (2021)
    -   Defined a domain ontology based on CIDOC-CRM
    -   Analysis of Flavio Biondo's "Italia Illustrata" (1474), a
        description of Italy's most important cities and communities

# References
- Bosse, A. (2019). Place. In H. Hotson & T. Wallnig (Eds.), Reassembling the Republic of Letters
in the Digital Age: Standards, Systems, Scholarship (p. 79–95). Göttingen: Göttingen University
Press.
- Ducatteeuw, V. (2021). Developing an Urban Gazetteer: A Semantic Web Database for
Humanities Data. Proceedings of the 5th ACM SIGSPATIAL International Workshop on
Geospatial Humanities, 36–39.
- Frank, I., & Barget, M. (2021, June 19). Ontology-based Modeling of Time, Places, Agents in
the Project DigiKAR (Digitale Kartenwerkstatt Altes Reich/Digital Map Lab Holy Roman
Empire). Data for History 2021: Modelling Time, Places, Agents - Poster and Abstracts. Data for
History 2021: Modelling Time, Places, Agents, Berlin.
- Gkadolou, E., & Prastacos, P. (2021). Historical Cartographic Information for Cultural
Heritage Applications in a Semantic Framework. Cartographica: The International Journal for
Geographic Information and Geovisualization, 56(4), 255–266.
- Görz, G., Seidl, C., & Thiering, M. (2021). Linked Biondo: Modelling Geographical Features
in Renaissance Texts and Maps. E-Perimetron, 16(2), 78–93.
- Grossner, K., Grunewald, S., & Mostern, R. (2022). Bringing places from the distant past to
the present: A report on the World Historical Gazetteer. International Journal on Digital
Libraries.
- Grossner, K., & Mostern, R. (2021). Linked Places in World Historical Gazetteer. Proceedings
of the 5th ACM SIGSPATIAL International Workshop on Geospatial Humanities, 40–43.
- Kahn, R., Isaksen, L., Barker, E., Simon, R., de Soto, P., & Vitale, V. (2021). Pelagios – Connecting Histories of Place. Part II: From Community to Association. International Journal
of Humanities and Arts Computing, 15(1–2), 85–100.
- Koho, M., Burrows, T., Hyvönen, E., Ikkala, E., Page, K., Ransom, L., Tuominen, J., Emery,
D., Fraas, M., Heller, B., Lewis, D., Morrison, A., Porte, G., Thomson, E., Velios, A., &
Wijsman, H. (2022). Harmonizing and publishing heterogeneous premodern manuscript
metadata as Linked Open Data. Journal of the Association for Information Science and
Technology, 73(2), 240–257.

**Footnotes**
[^1]: Linked Traces:
    <https://github.com/LinkedPasts/linked-traces-format> (Accessed:
    21.10.2022); Linked Places:
    <https://github.com/LinkedPasts/linked-places-format> (Accessed:
    21.10.2022). Note on the project status of the traces format: The
    Linked Pasts format is a work in progress project, and there is
    still only a small number of data available in the WHG that provide
    information about trace-data.

[^2]: Arches is an open-source data management platform for heritage
    data: <https://www.archesproject.org/> (accessed: 22.10.2022).

