[[_TOC_]]
# Introduction - About This Guide

This guide is a companion, product, and result of the master thesis
*"Modeling Place with Graphs and Networks."*[^1] It is based on the
interpretation and critical reflection of the presented research
analysis. It shows some best practices in Spatial Humanities regarding
modeling spatial and platial information with graph- and network-related
methods. The guide focuses on incorporating a Human Geographic
perspective on Place[^2] in Spatial Humanities projects, especially for
students that have never dealt with Place before. Although it can help
in several scenarios regarding research in Spatial Humanities, it
focuses on modeling Place. The intention of this guide is not to be
dogmatic and state that from now on, Place must be an integral part of
every project in Spatial Humanities. Instead, the guide wants to offer
an additional set of Place-glasses to look through to get a different
contextual perspective on research projects in Spatial Humanities.

## The content of this guide

Firstly, the guide provides a user-oriented overview of the most common
modeling approaches applied in Spatial Humanities. Secondly, the guide
focuses on the proposed approach to Place, presented in the master
thesis, to have the most promise in modeling Place from a Human
Geographic perspective. To clarify the meaning of Place, a short
introduction to Place from the Human Geographic perspective is included
to point out the reason for this guide. Since modeling involves
something to be modeled with something, the two aspects touching that
topic (tools and data) are also part of this guide. Tools and data are
analyzed with a specific focus on modeling Place with graphs.

## The Motivation for this Guide

The motivation for this guide is fourfold. The first one results from
the master thesis, realizing that the Place concept from Human Geography
differs greatly from the ones used in many research projects in Spatial
Humanities. But the author of the thesis sees great promise to adapt the
Human Geographic understanding of Place in Spatial Humanities for a
deeper understanding of the sociocultural production of platial
constructs. Therefore, this guide wants to highlight these modeling
methods already integrated into Spatial Humanities practice that allow a
deeper understanding of Place.

Secondly, there is no general overview of practical approaches to place
in Spatial Humanities. For the creation of this guide and the master
thesis, a great deal of literature research was involved. So, it is
intended to be a shortcut and road map to works in Spatial Humanities
dealing with Place.

Thirdly, in the studies of Digital Humanities, Place is mainly presented
as a georeferenced location. However, reducing Place to coordinates
means losing a lot of informational richness that could be easily added
to any research project in Digital and Spatial Humanities. This guide
wants to show that including Place in research projects offers an
excellent opportunity to add additional and valuable information.
Including Place in projects can tell a lot about many circumstances that
are of interest for research in Digital and Spatial Humanities. The
guide intends to show that a Place is more than a location.

And last, the systematic analysis presented in the master thesis only
provides an overview of the primary outcomes of interest. The
information presented is more abstract and theoretical, focusing on the
main characteristics that could be extracted from the massive amount of
data gathered from all the projects analyzed. It would be of no interest
for anybody to read a detailed analysis of 30 projects. The amount of
information presented in such an analysis would be too overwhelming and
not easy to digest for anybody. While reading the results, it would be
valid to ask, what do graphs even have to do with most of the outcomes
presented? In many cases, graphs are mentioned only indirectly.
Therefore, this guide explicitly focuses on the functional role of
graphs in Spatial Humanities projects.

## For whom is this guide?

The guide is for anybody interested in Place, Spatial Humanities,
Digital Humanities, graphs, and networks. It is intended to reach as
many people as possible so that they get a general overview of modeling
and methodological practice of Place, specifically in Spatial
Humanities. One target group is students who are about to do projects in
Spatial Humanities for the first time. The guide provides an easy start
to dive into the diversity of approaches. It gives them some practical
examples to try out and get a first feeling of what Place research in
Spatial Humanities involves. It is for those who want inspiration and
ideas about what other studies have done in Spatial Humanities.

## How to Use this Guide

To use the guide, the reader can just look at the different main topics
and browse through the mix of statements, information, examples, and
questions that might come up in a research project. Additionally,
references to resources are added for further information that would be
out of the scope of this little guide. Each chapter is in its own repository. 
This way the reader can jump right to the chapter interested in. Examples are included in this
guide to make the practical implications of some statements more
elusive. The examples were created with different tools, depending on
the applicability to the simulated use case. Some were developed with
the strongly typed graph database typeDB and its corresponding query
language typeQL and executed in the integrated development environment
typeDB studio.[^3] Other examples were created with standard tools used
in many Spatial Humanities projects, like Gephi and Recogito.[^4] The examples are also in their corresponding chapter-repository. The examples can be downloaded and executed in their corresponding tool and the reader should achive the same results as presented in the guide. This
guide tries to work with as many visualizations as possible so that the
reader does not have to do a tutorial on the chosen graph database
language prior to using this guide. The complete guide and thesis as a PDF document can be found in the [Document Repository](./07-Documents).

The author does not make any claims that this guide is by any means
complete or correct in any way. The content results from the literature
analysis presented in the related master thesis and bits and pieces
collected here and there while writing the thesis.


**Footnotes**
[^1]: If you are interested in the way the guide was created, please
    refer to the master thesis, there you can find a chapter on it, the
    results the guide is based on, as well as some information on the
    motivation and intention of this guide. You can find the thesis in the docs repository

[^2]: Throughout the thesis the spelling Place is used to refer to the
    Human Geography concept, and to differentiate it from the common,
    everyday understanding and usage of place and what is related to it.

[^3]: typeDB, typeQL, and typeDB studio website: <https://vaticle.com/>
    (accessed: 16.09.2022).

[^4]: For a more detailed comment on both these tools see also the
    section on tools and technologies. Gephi is a graph analysis and
    visualization tool (website: <https://gephi.org/>; accessed:
    20.10.2022). Recogito is a semantic annotation tool (website:
    https://recogito.pelagios.org/; accessed: 20.10.2022).
