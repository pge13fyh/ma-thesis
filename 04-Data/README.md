[[_TOC_]]
# Data for Place Research

The way data is and can be used in research projects is an extensive
area and depends heavily on the type/goal/aim of a research project.
Therefore, this section refrains from making very explicit statements
but gives some guiding notes that could be helpful for many different
research projects.

## Characteristics of data in the humanities

-   Vagueness
    -   The degree of the preciseness of the information is not
        guaranteed
-   Uncertainty
    -   Historical sources are not bound to be the phenomenological
        truth
-   Incomplete
    -   There is a great chance involved in using historical data that
        not the complete source survived through time
-   It might only represent one perspective
    -   Only a tiny proportion of people created the information
        available from the past
    -   Suppose a project is interested in the everyday life situation
        in Greco-Roman times, and there is a source from a philosopher
        that describes their condition. In that case, one should be
        aware that these are the accounts from the philosopher's
        perspective.

**Approaches to these data characteristics:**

-   One way to approach these characteristics of data in the humanities
    is by applying semantic web and LOD data technologies (e.g.,
    scraping information from knowledge graphs):
    -   To be able to meet the gaps with information from additional
        data sources
    -   Infer blanks in data sources with information from other data
        sources
    -   To find evidence about information found in the data
-   Add tags that concern these characteristics
    -   E.g., tags that classify the degree of (un-)certainty of the
        information in the data
-   Ask others to verify the information
    -   E.g., with crowdsourcing that can confirm or edit certain
        information

*Table : Overview of Project Data Source Types.*

| General type of Data Sources | Subcategory of Data Source      | Example of kind of Data Source            | Example or research project                                      |
|------------------------------|---------------------------------|-------------------------------------------|------------------------------------------------------------------|
| **Textual data**              | Narratives                      | Interviews, Diaries                       | Cole and Hahmann (2019)                                          |
|                              | Letters                         | Charles Darwin’s Letter to Syms Covington | Center of Urban History (2018)                                   |
|                              | “Official” archival Records     | Marriage/Death/Birth Registers            | Digital History (2021; van Wissen, Zamberlini, van den Heuvel)   |
|                              | Manuscripts (handwritten books) |                                           | Koho et al. (2022) <br>Burrows (2017)                            |
| **Spatial representations**     | Maps                            | Ancient Map from X                        | Murrieta-Flores et al. (2019)<br> Gkadolou and Practascos (2021) |
|                              | Gazetteers                      | World Historical Gazetteer                | Ducatteeuw (2021)                                                |
|                              | Spatial Narratives              | Portopans                                 | Palladino (2020)                                                 |
| **Visual**                       | Photographs                     |                                           |  Novak et al. (2014)                                             |
|                              | Drawings                        | Technical Drawings <br><br>Paintings      | Van Den Heuvel (2015)                                            |

## Characteristics of data types and their modeling involved

### Textual data

-   Hint for the modeling task: Take advantage of the structure that is
    already present in the linguistic structure of texts
    -   Classes for subjects, adjectives, ...
    -   Classes of sentence types: Relative clauses, questions, ...
-   Publication worth looking at:
    -   Palladino (2021)
-   Understands language as a knowledge system and transfers this to
    create a meta-data-model for a graph-based representation of text
    and in the language codified information

### Letters

-   The different places often mentioned in letters
    -   The place where the letter is sent to
    -   The place from where the letter came from
    -   The places mentioned in the letter
-   The difference in the times mentioned
    -   The time of sending the letter is different from the time that
        the letter arrived at the receiver
-   Historical letters might use an address not as precise as modern
    addresses
    -   The tower
    -   Next to the great oak
-   Just a little reminder: Envelopes are a relatively new thing. In
    early modern times, envelopes were not used
-   A project dealing with letters: Bosse (2019)

### Maps

-   What is essential about the map for the research question: the
    content of the map or the layout of the map?
-   Publication worth looking at that deal with (historical) maps in one
    way or another:
    -   Murrieta-Flores et al. (2019): RCC applied to historical maps
    -   Gkadolou & Prastacos (2021): Create a data management system for
        historical maps
    -   Mai et al. (2022): Create a tool to automatically enrich maps
        semantically

### Images

-   A tool that allows annotating images: Recogito

## Place information in data and what to be aware of:

-   Places-names might change
-   Places have different names, not only in other languages but also in
    the same language. For example, Leipzig and Leipsch (in a dialect)
-   Place locations can also change over time
    -   Towns are growing with time or are getting smaller
-   Places can disappear
-   The place might be known, but the exact location is
    unknown/fictional/...
    -   Heaven, Atlantis, ...
-   Places are not mentioned explicitly but implicitly
    -   Example: "I was in a town in France this summer. It was lovely
        there. All the time, while walking around, I always saw this big
        tower that looked like a big needle pin."
    -   This example talks about Paris, which might be obvious to the
        reader, but the place name Paris is not mentioned once.
        []{#_Toc116661250 .anchor}


*Table: List of common Gazetteers. Gazetteers list place names and additional information.*

| Namne                                     | Description                                                             | Link                                                   |
|-------------------------------------------|-------------------------------------------------------------------------|--------------------------------------------------------|
| World Historical Gazetteer (WHG)          | Gazetteer of historical places                                          | https://whgazetteer.org/                               |
| Pleiades                                  | Gazetteer of ancient places                                             | https://pleiades.stoa.org/                             |
| GeoNames                                  | Gazetteer of geographic names <br> NOTE: only contemporary place names! | https://www.geonames.org/                              |
| Getty Thesaurus of Geographic Names (GTN) | A structured vocabulary of names and other information about places     | https://www.getty.edu/research/tools/vocabularies/tgn/ |


*Table: Overview of Linked Open Data portals.*
| Name                                           | Description                                                                                                                                       | Link                                        | Associated Publication                       |
|------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|----------------------------------------------|
| Mapping Manuscript Migration                   | Network correspondence analysis of the intellectuals of the Age of Enlightenment of the 17th-18th century                                         | https://mappingmanuscriptmigrations.org/en/ | Hyvönen et al. (2021) <br>Koho et al. (2022) |
| Early Modern Letters Online                    | The collection of letters from the early modern period allows one to track back the correspondence network. Includes some provenance information. | http://emlo.bodleian.ox.ac.uk/advanced      | Uchacz (2019)                                |
| Candia Maps                                    | Database of historical maps from Crete, Greece.                                                                                                   | https://candiamaps.iacm.forth.gr/search     | Gkadolou & Prastacos (2021)                  |
| Encyclopedia of Romantic Nationalism in Europe | Collection of articles and historical resources to trace back the rise of romantic nationalism in the 19th century in Europe                      | https://ernie.uva.nl/viewer.p/21/56         |                                              |

# References
- Bosse, A. (2019). Place. In H. Hotson & T. Wallnig (Eds.), Reassembling the Republic of Letters
in the Digital Age: Standards, Systems, Scholarship (p. 79–95). Göttingen: Göttingen University
Press.
- Burrows, T. (2017). The History and Provenance of Manuscripts in the Collection of Sir
Thomas Phillipps: New Approaches to Digital Representation. Speculum, 92(S1), S39–S64.
- Center for Urban History, Lviv. (2018). Imaginary Map of the Literary Lviv (±1939). Retrieved
August, 22, 2022, from https://lia.lvivcenter.org/en/projects/litlviv/intro
- Cole, T., & Hahmann, T. (2019). Geographies of the Holocaust: Experiments in GIS, QSR,
and Graph Representations. International Journal of Humanities & Arts Computing: A Journal
of Digital Humanities, 13(1–2), 39–52.
- Digital History. (2021, November 1). Leon van Wissen/Veruska Zamborlini/Charles van
den Heuvel: Toward an ontology for archival resources. Modelling persons, objects and
places in the Golden Agents research infrastructure [Billet]. Digital History Berlin. Retrieved
August, 20, 2022, from https://dhistory.hypotheses.org/361
- Ducatteeuw, V. (2021). Developing an Urban Gazetteer: A Semantic Web Database for
Humanities Data. Proceedings of the 5th ACM SIGSPATIAL International Workshop on
Geospatial Humanities, 36–39.
- Gkadolou, E., & Prastacos, P. (2021). Historical Cartographic Information for Cultural
Heritage Applications in a Semantic Framework. Cartographica: The International Journal for
Geographic Information and Geovisualization, 56(4), 255–266.
-Hyvönen, E., Ikkala, E., Koho, M., Tuominen, J., Burrows, T., Ransom, L., & Wijsman, H.
(2021). Mapping Manuscript Migrations on the Semantic Web: A Semantic Portal and
Linked Open Data Service for Premodern Manuscript Research. In A. Hotho, E. Blomqvist,
S. Dietze, A. Fokoue, Y. Ding, P. Barnaghi, A. Haller, M. Dragoni, & H. Alani (Eds.), The
Semantic Web – ISWC 2021 (Vol. 12922, p. 615–630). Basel: Springer International Publishing.
- Koho, M., Burrows, T., Hyvönen, E., Ikkala, E., Page, K., Ransom, L., Tuominen, J., Emery,
D., Fraas, M., Heller, B., Lewis, D., Morrison, A., Porte, G., Thomson, E., Velios, A., &
Wijsman, H. (2022). Harmonizing and publishing heterogeneous premodern manuscript
metadata as Linked Open Data. Journal of the Association for Information Science and
Technology, 73(2), 240–257.
- Mai, G., Huang, W., Cai, L., Zhu, R., & Lao, N. (2022). Narrative Cartography with
Knowledge Graphs. Journal of Geovisualization and Spatial Analysis, 6(4).
- Murrieta-Flores, P., Favila-Vázquez, M., & Flores-Morán, A. (2019). Spatial Humanities 3.0:
Qualitative Spatial Representation and Semantic Triples as New Means of Exploration of
Complex Indigenous Spatial Representations in Sixteenth Century Early Colonial Mexican
Maps. International Journal of Humanities & Arts Computing: A Journal of Digital Humanities,
13(1–2), 53–68.
- Novak, J., Micheel, I., Melenhorst, M., Wieneke, L., Düring, M., Morón, J. G., Pasini, C.,
Tagliasacchi, M., & Fraternali, P. (2014). HistoGraph – a visualization tool for collaborative
analysis of networks from historical social multimedia collections. 18th International
Conference on Information Visualisation, 241–250.
- Palladino, C. (2021). Mapping the unmapped: Transmedial representation of premodern
geographies. Berichte Geographie Und Landeskunde, 94(2), 139–160.
-Uchacz, T. H. (2019). Early Modern Letters Online (EMLO). Isis, 110(3), 567–569.
- Van Den Heuvel, C. (2015). Mapping Knowledge Exchange in Early Modern Europe:
Intellectual and Technological Geographies and Network Representations. International
Journal of Humanities and Arts Computing, 9(1), 95–114.
https://doi.org/10.3366/ijhac.2015.0140
