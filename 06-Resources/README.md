
# Resources for Graphs and Networks in Research

Here are some lists of resources that are worth looking at. The
resources can help to get some additional inspiration and information.
They are divided into different categories and types of resources.

## List of Journals about Spatial Humanities and Graphs and Networks.
| Name          | Link           |
| ------------- |:-------------:|
|The Journal of Historical Network Research|https://historicalnetworkresearch.org/jhnr/|
|Digital Humanities Quarterly|http://www.digitalhumanities.org/dhq/|
|International Journal of Humanities and Arts Computing|https://www.euppublishing.com/loi/ijhac|
|Digital scholarship in the Humanities|https://academic.oup.com/dsh|

## List of Spatial/Digital Humanities Conferences.
| Name          | Link           |
| ------------- |:-------------:|
|Graphentechnologien in den digitalen Geisteswissenschaften|https://graphentechnologien.hypotheses.org/tagungen|
|DHd. Digital Humanities im deutschprachigen Raum|https://dig-hum.de/aktuelles|
|Historical Network Research Conference|https://historicalnetworkresearch.org/hnr-events/|

## List of Online Resources about Spatial/Digital Humanities, Graphs, and Networks.
| Name          | Link           |
| ------------- |:-------------:|
|Graphen und Netzwerke. Eine AG des Verbandes Digital Humanities im deutschprachigen Raum|https://graphentechnologien.hypotheses.org/|
|Pelagios network|https://pelagios.org/|

## List of general publications that deal with Spatial Humanities worth looking at.
| Publication          |
| ------------- |
|Ahnert, R., Ahnert, S. E., Coleman, C. N., & Weingart, S. B. (Eds.). (2020). The Network Turn: Changing Perspectives in the Humanities (1st ed.). Cambridge: Cambridge University Press.|
|Bavaj, R., Lawson, K., & Struck, B. (Eds.). (2022). Doing spatial history. Routledge.|
|Bodenhamer, D. J., Corrigan, J., & Harris, T. M. (Eds.). (2010). The spatial humanities: GIS and the future of humanities scholarship. Bloomington: Indiana University Press.|
|Gregory, I., DeBats, D., & Lafreniere, D. (Eds.). (2018). The Routledge Companion to Spatial History (1st ed.). London: Routledge.|
|Gregory, I. N., & Geddes, A. (Eds.). (2014). Toward spatial humanities: Historical GIS and spatial history. Bloomington: Indiana University Press.|


