[[_TOC_]]

# Short Introduction to Place

This short introduction presents the underlying Place understanding
primarily used in this guide originating in Human Geography.[^1] It is a
construct deeply entangled in most of our everyday communication. We all
seem to know what a place is but being asked to give a crisp definition
of place would make some people probably startle for a moment. What is a
place? How would we define it? Maybe a home, a safe space, a location?
But is there not a saying like "Home is where my heart is" and is home
not clearly a place? Taking the expression literally, the location of
the heart constantly changes with every movement person makes. Where is
the location of the home then? This example shows that place is
complicated to define. One aspect becomes evident from this example:
There is a common-sense understanding of what place is, which needs no
further explanation for others to understand place.

Human Geographers use this common-sense concept to describe the human
experience of spatial events (Blaschke et al., 2018, p. 2). In this
guide\'s context, it is essential to understand the use of Place
concepts in Human Geography to make it tangible in research. The Place
construct is used in Human Geography to analyze how people interact with
their surroundings and create meaning for them. It can be seen as a
specific approach to studying how people make sense of their
surroundings and how cultural practices are done. The Place constructs
work as a means for researchers and people, in general, to communicate
about it, and in Human Geography, a specific Place construct expresses a
particular understanding and focus on Place (Janowicz, 2009, p. 57).

This guide presents specific methods with graphs that can help to bring
the notion of Place in research projects in Spatial Humanities. Many
projects in Spatial Humanities show a particular awareness that Place is
more than just a name or a location. Nevertheless, many researchers
reduce Place to one or the other in their project. Reducing Place to
coordinates implies that a lot of the richness a Human Geographic
understanding of Place could offer is left aside. As mentioned in the
introduction of this guide, it is not the intention to dictate that
every project in Spatial Humanities must use and investigate Place, as
presented in the master thesis and this guide. Instead, it should be
understood as an inspiration to use the works done in Human Geography
and investigate what it offers in the context of Spatial Humanities.

# References
- Blaschke, T., Merschdorf, H., Cabrera-Barona, P., Gao, S., Papadakis, E., & Kovacs-Györi, A. (2018). Place versus Space: From Points, Lines and Polygons in GIS to Place-Based Representations Reflecting Language and Culture. ISPRS International Journal of Geo-
Information, 7(11), 1–26. 
- Cresswell, T. (2004). Place: A short introduction. Malden: Blackwell Pub.
- Hamzei, E., Winter, S., & Tomko, M. (2020). Place facets: A systematic literature review.
Spatial Cognition & Computation, 20(1), 33–81. 
- Janowicz, K. (2009). The role of place for the spatial referencing of heritage data. The Cultural Heritage of Historic European Cities and Public Participatory GIS Workshop, 9, 57–67.


[^1]: For a more detailed description of the Human Geographic concept of
    Place and the many problems involved in conceptualizing it for
    digital research, please refer to chapter 2.1 of the master thesis.
    Additionally see the following publications that are introducing
    and/or reviewing Place see the following publications: (Cresswell,
    2004; Hamzei et al., 2020)
