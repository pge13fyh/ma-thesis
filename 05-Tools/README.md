
[[_TOC_]]
# Tools for Graphs and Networks in Spatial Humanities 

Like the previous section on data, this section will not go into detail
with each tool because the use case can differ significantly based on
the underlying research question. Instead, this section highlights some
use cases for which specific tools are explicitly developed.

## Overview -- List of Some Common Graph-Related-Tools

| Name    | Description         | Link          | Projects  using them| Foccus use    |
|---------|:-------------------:|:-------------:|:---------:|:------:|
| Nodegoat| Data management, data analysis, and data visualization, web-based research environment. All-inclusive research environment | https://nodegoat.net/ | Burrows (2017); van Bree & Kessels (2015) |Data modeling/management/annotation/visualization |
|Recogito|Semantic annotation tool for text s and images;  Nice thing: if the place is found in a gazetteer, the place is further tagged with controlled vocabulary|https://recogito.pelagios.org/|Foka et al. (2022); Görz et al. (2021); Horne (2021)|Annotation|
|Gephi|Graph visualization and analysis tool|https://gephi.org/|Ferreira-Lopes & Pinto-Puerto (2018)|Visualization, graph theory calculations|
|Grazce|Graph Content Editor|https://lod.academy/site/tools/digicademy/grace|Not available yet|Annotation|
|ArcGIS|Geographic information software. Closed source and not free|https://www.arcgis.com/index.html|Ferreira-Lopes et al. (2016); Ferreira-Lopes & Pinto-Puerto (2018)|Geo-Analysis|
|QGIS|Free and open-source geographic information software|https://www.qgis.org/de/site/||Geo-Analysis|
|Codex|Tool to semantically annotate the data that allows presenting the results in a graph|https://neo4j.com/blog/building-graph-history-codex/|Neill & Kuczera (2019); Palladino et al. (2020)|Annotation|
|Palladio|Data exploration tool. Offers different data visualizations|https://hdlab.stanford.edu/palladio/|Conroy (2021)|Good to explore data on the fly|
|histoGraph|Graph-based data exploration tool|http://histograph.eu/|Novak et al. (2014)|Focus on exploration, bridging close and distant reading|
|WissKI|Virtual Research environment|https://sempub.ub.uni-heidelberg.de/wisski_projekte/de|Görz et al. (2021)||

## General types of tools

### Exploration and analysis tool

These tools are all about exploring data and the structures that are
part of the data itself. Such tools allow users to zoom in and out,
browse from one node to another, and visualize the information in many
ways. The kinds of analysis and explorations offered by the tools are
often already pre-defined by the tools. Two tools that focus on that are
histoGraph and Palladio. The second one has been extensively reviewed by
Conroy (2021).

### Semantic enrichment tools

These kinds of tools focus on annotating data with additional data. One
was already introduced in the examples created for this guide: Recogito.
Recogito allows to tag data and already differentiates between the three
types: place, person, and event. For the places, Recogito is connected
to several digital gazetteers that allow the extraction of information
from these sources as well. Additionally, as was already extensively
used to create the field maps, Recogito allows to add relations between
tags. Another tool focusing on that use is Codex.

![Recogito Workspace](./image27.png)

*Figure: Recogito Workspace.*

### Research environments

These tools aim to be the researchers\' dream and be everything that
makes a researcher\'s heart beat faster. It starts with offering ways to
model, enter, edit, visualize, and analyze data. So basically,
everything that needs to be done in a research project should be
possible in the research environment. Two of these tools are WissKI
(Association for semantic data processing (IGSD e.V.), 2021) and
nodegoat (van Bree & Kessels, 2013).

### Geoinformation System/Software (GIS)

GIS focus on the use, visualization, and analysis of geographic data.
GIS allows so many different analyses that it would be too much to list
here. One worth mentioning here is the network analysis. Think of a road
network, and network analysis in GIS allows, for example, the
calculation of service areas within reach of a particular district. Or
calculate the amount of flow (e.g., of cars, public transportation) that
can go through the network within a certain amount of time or other
factors like traffic. Two of the most known GIS tools are the closed
source and non-free ArcGIS (Esri, n.d.) and the open source and free
QGIS (QGIS Association, n.d.).

# References
- Association for semantic data processing (IGSD e.V.). (2021). WissKi. Retrieved October, 25,
2022, from https://wiss-ki.eu/
- Burrows, T. (2017). The History and Provenance of Manuscripts in the Collection of Sir
Thomas Phillipps: New Approaches to Digital Representation. Speculum, 92(S1), S39–S64.
- Conroy, M. (2021). Networks, Maps, and Time: Visualizing Historical Networks Using
Palladio. DHQ: Digital Humanities Quarterly, 15(1).
- Esri. (n.d.). Esri Produkte. Retrieved October 25, 2022, from https://www.esri.de/dede/
arcgis/produkte/uebersicht
- Ferreira-Lopes, P., & Pinto-Puerto, F. (2018). GIS and graph models for social, temporal and
spatial digital analysis in heritage: The case-study of ancient Kingdom of Seville Late Gothic
production. Digital Applications in Archaeology and Cultural Heritage, 9
- Ferreira-Lopes, P., Pinto-Puerto, F., Jimenez Mavillard, A., & Suarez, J. L. (2016). Seeing Andalucia’s Late Gothic heritage through GIS and Graphs. Digital Humanities 2016:
Conference Abstracts., 501–504.
- Foka, A., Konstantinidou, K., Mostofian, N., Talatas, L., Kiesling, J. B., Barker, E.,
Demiroglu, O. C., Palm, K., McMeekin, D. A., & Vekselius, J. (2022). Heritage metadata A
digital Periegesis. In K. Golub & Y.-H. Liu (Eds.), Information and Knowledge Organisation in
Digital Humanities: Global Perspectives (p. 227–242). London: Routledge.
- Görz, G., Seidl, C., & Thiering, M. (2021). Linked Biondo: Modelling Geographical Features
in Renaissance Texts and Maps. E-Perimetron, 16(2), 78–93.
- Horne, R. (2021). Digital Tools and Ancient Empires: Using Network Analysis and
Geographic Information Systems to Study Imperial Networks in Hellenistic Anatolia.
Journal of World History, 32(2), 321–343.
-Neill, I., & Kuczera, A. (2019). The Codex – an Atlas of Relations. In A. Kuczera, T.
Wübbena, & T. Kollatz (Eds.), Modellierung des Zweifels – Schlüsselideen und -konzepte zur
graphbasierten Modellierung von Unsicherheiten: Vol. = Zeitschrift für digitale
Geisteswissenschaften / Sonderbände,4 (1.0, p. 4).
- Novak, J., Micheel, I., Melenhorst, M., Wieneke, L., Düring, M., Morón, J. G., Pasini, C.,
Tagliasacchi, M., & Fraternali, P. (2014). HistoGraph – a visualization tool for collaborative
analysis of networks from historical social multimedia collections. 18th International
Conference on Information Visualisation, 241–250.
- Palladino, C., Kuczera, A., & Neill, I. (2020, February 21). Modeling the ‘Unthought.’ Graph
Technologies in the Humanities - Proceedings 2020. Graph Technologies in the Humanities,
Vienna.
- QGIS Association. (n.d.). QGIS Geographic Information System. Retrieved October 25, 2022,
from https://www.qgis.org/de/site/
- van Bree, P., & Kessels, G. (2015). Mapping Memory Landscapes in nodegoat. In L. M.
Aiello & D. McFarland (Eds.), Social Informatics (Vol. 8852, pp. 274–278). Basel: Springer
International Publishing.
